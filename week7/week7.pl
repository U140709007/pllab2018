use strict;
use warnings;

print "Hello World!\n";

my $scaler_variable = 5;

print 'The value of $scaler_variable is ' . "$scaler_variable" . "\n";

$scaler_variable = "this is a string";

print 'The value of $scaler_variable is now ' . "$scaler_variable" . "\n";

my @a = (10, 20, 30);

print "@a\n";

@a = (10, "this is a string", 5.9, $scaler_variable);

print "@a\n";
